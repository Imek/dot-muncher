#ifndef GHOST_MANAGER_H
#define GHOST_MANAGER_H

#include <vector>
#include "Ghost.h"
#include "Avatar.h"
#include "Drawer.h"

// Ghost vector typedef for brevity.
typedef std::vector<Ghost*> GhostVec;

// Class to govern the ghosts in pac man - creation, destroying, and drawing.
class GhostManager
{
public:
	GhostManager();
	~GhostManager();

	// Initialisation, including loading any assets.
	bool Init(Drawer& drawer, const World& world);

	// Perform state update given time passed since last update.
	void Update(float timeSecs, World& world, Avatar& avatar);

	// Draw the ghosts in the game world.
	void Draw(Drawer& drawer) const;

	// Trigger when the game stage is reset (e.g. player death)
	void Reset();

	// Trigger when the protagonist eats a big dot (starts FRIGHTENED state)
	void OnEatenBigDot();

	// Provide reference access to the ghost list.
	const GhostVec& GetGhosts() { return m_ghosts; }
	// Provide reference access to an individual ghost by type.
	// This implementation assumes one ghost of each type, at the correct index!
	const Ghost& GetGhost(Ghost::GhostType ghostType) { return *(m_ghosts[ghostType]); }

private:
	// Private dummy copy/assignment operators for safety, since we're managing memory; we don't
	// want or need this functionality in this class.
	GhostManager(const GhostManager&);
	GhostManager& operator=(const GhostManager&);

	// Called by Ghost::Update if this ghost should have its target updated.
	void UpdateGhostTarget(const World& world, const Avatar& avatar, Ghost& ghost, bool isScattering);

	// Our ghosts, created on Init.
	GhostVec m_ghosts;

	// Time counter variables.
	float m_frightenedCounter;
	float m_scatterCounter;
	int m_scatterStateIndex;
};

#endif // GHOST_MANAGER_H
