#include "World.h"
#include <iostream>
#include <algorithm>
#include <sstream>
#include <fstream>
#include <string>

#include "Drawer.h"
#include "MovableEntity.h"
#include "WorldTile.h"

static const std::string MAP_FILE = "data/maps/map.txt";

World::World(void)
: m_mazeImageID(-1)
, m_dotImageID(-1)
, m_bigDotImageID(-1)
, m_cherryImageID(-1)
, m_sizeX(0)
, m_sizeY(0)
, m_numDots(0)
, m_worldTiles(NULL)
, m_cherry(NULL)
{
}

World::~World(void)
{
	for (int tileIndex = 0; tileIndex < m_sizeX*m_sizeY; ++tileIndex)
		delete m_worldTiles[tileIndex];
	delete[] m_worldTiles;

	for (std::list<Entity*>::iterator it = m_dots.begin(); it != m_dots.end(); it++)
		delete *it;

	for (std::list<Entity*>::iterator it = m_bigDots.begin(); it != m_bigDots.end(); it++)
		delete *it;

	delete m_cherry;
}

bool World::Init(Drawer& drawer)
{
	// Init includes pre-loading world images.
	m_mazeImageID = drawer.LoadImage("maze.png");
	m_dotImageID = drawer.LoadImage("small_dot_32.png");
	m_bigDotImageID = drawer.LoadImage("big_dot_32.png");
	m_cherryImageID = drawer.LoadImage("cherry_32.png");

	if (!InitMapSize())
		return false;
	else
		return InitMapObjects();
}

bool World::InitMapSize()
{
	// Make sure we're in the expected initial state (not called multiple times!)
	assert(m_sizeX == 0 && m_sizeY == 0);
	assert(m_worldTiles == NULL);
	// Size and validate map in the first pass.
	std::string line;
	std::ifstream mapFile(MAP_FILE);
	if (mapFile.is_open())
	{
		int lineIndex = 0;
		while (!mapFile.eof())
		{
			// Note that this code just skips any empty lines and uses the
			// smallest line length as the dimension.
			std::getline(mapFile, line);
			if (line.length() == 0)
				continue;
			
			if (m_sizeX == 0)
				m_sizeX = line.length();
			else if (line.length() < (unsigned)m_sizeX)
				m_sizeX = line.length(); // Inconsistent line lengths

			m_sizeY++;
		}
		mapFile.close();

		if (m_sizeX == 0 || m_sizeY == 0)
			return false; // Empty file

		assert(m_sizeX > 0 && m_sizeY > 0);
		m_worldTiles = new WorldTile*[m_sizeX*m_sizeY];

		return true;
	}
	else
	{
		// Failed due to no map file found.
		return false; 
	}
}

bool World::InitMapObjects()
{
	assert(m_sizeX > 0 && m_sizeY > 0);

	std::string line;
	std::ifstream mapFile(MAP_FILE);
	if (mapFile.is_open())
	{
		int lineIndex = 0;
		while (!mapFile.eof())
		{
			std::getline(mapFile, line);
			// Note that we ignore empty lines and extra characters as in InitMapSize.
			if (line.length() == 0)
				continue;
			for (unsigned int i = 0; i < (unsigned)m_sizeX; i++)
			{
				// We always create a WorldTile. 
				InitWorldTile(i, lineIndex, line[i]);
				// Also determine whether to create other entities.
				switch (line[i])
				{
				case '.':
					InitDot(i, lineIndex);
					break;
				case 'o':
					InitBigDot(i, lineIndex);
					break;
				case '*':
					InitCherry(i, lineIndex);
					break;
				}
			}
			lineIndex++;
		}
		mapFile.close();
		return true;
	}
	else
	{
		// Failed due to no map file found.
		return false; 
	}
}

void World::InitWorldTile(int xCoord, int yCoord, const char& tileChar)
{
	assert(xCoord >= 0 && xCoord < m_sizeX);
	assert(yCoord >= 0 && yCoord < m_sizeY);
	WorldTile* tile = new WorldTile(
		xCoord, yCoord, // Coordinates
		(tileChar == 'x'), // Blocking
		(tileChar == '='), // Tunnel
		(tileChar == '-') // Ghost house
	);
	m_worldTiles[yCoord*m_sizeX + xCoord] = tile;
}

void World::InitDot(int xCoord, int yCoord)
{
	Entity* dot = new Entity((float)xCoord, (float)yCoord, m_dotImageID);
	m_dots.push_back(dot);
	m_numDots++;
}

void World::InitBigDot(int xCoord, int yCoord)
{
	Entity* dot = new Entity((float)xCoord, (float)yCoord, m_bigDotImageID);
	m_bigDots.push_back(dot);
	m_numDots++;
}

void World::InitCherry(int xCoord, int yCoord)
{
	// Make the cherry entity.
	// NOTE: Since the cherry is actually between two tiles, the rule is for map.txt
	// to specify the left-hand tile.
	m_cherry = new Entity((float)xCoord+0.5f, (float)yCoord, m_cherryImageID, false);
}

void World::Draw(Drawer& drawer) const
{
	// Draw the world. Pretty straightforward; just draw the whole background and entities on top
	// every time.
	// NOTE: Could optimise by being economical about what sections get re-drawn, based
	// on what has changed.
	drawer.Draw(m_mazeImageID);

	for(std::list<Entity*>::const_iterator it = m_dots.begin(); it != m_dots.end(); it++)
	{
		Entity* dot = *it;
		dot->Draw(drawer);
	}

	for(std::list<Entity*>::const_iterator it = m_bigDots.begin(); it != m_bigDots.end(); it++)
	{
		Entity* dot = *it;
		dot->Draw(drawer);
	}

	m_cherry->Draw(drawer);
}

bool World::TileIsInBounds(int xCoord, int yCoord) const
{
	const WorldTile* tile = GetTile(xCoord, yCoord);
	return tile != NULL;
}

bool World::TileIsValid(int xCoord, int yCoord) const
{
	const WorldTile* tile = GetTile(xCoord, yCoord);
	return tile != NULL && !tile->m_isBlocking;
}

bool World::TileIsTunnel(int xCoord, int yCoord) const
{
	const WorldTile* tile = GetTile(xCoord, yCoord);
	return tile != NULL && tile->m_isTunnel;
}

bool World::TileIsGhostHouse(int xCoord, int yCoord) const
{
	const WorldTile* tile = GetTile(xCoord, yCoord);
	return tile != NULL && tile->m_isGhostHouse;
}

// XXX: Could optimise the below code by caching static entity coordinates on World and only
// checking intersection if the entities are within +-1 X/Y of each other. Since the world is
// relatively simple, though, the game seems to perform OK as-is.
bool World::CheckIntersectedDot(const Entity& entity)
{
	for(std::list<Entity*>::iterator it = m_dots.begin(); it != m_dots.end(); it++)
	{
		Entity* dot = *it;
		if (dot->Intersect(entity))
		{
			m_dots.remove(dot);
			delete dot;
			return true;
		}
	}
	return false;
}

bool World::CheckIntersectedBigDot(const Entity& entity)
{
	for (std::list<Entity*>::iterator it = m_bigDots.begin(); it != m_bigDots.end(); it++)
	{
		Entity* dot = *it;
		if (dot->Intersect(entity))
		{
			m_bigDots.remove(dot);
			delete dot;
			return true;
		}
	}

	return false;
}

bool World::CheckIntersectedCherry(const Entity& entity)
{
	// Perform the check to intersect the bonus fruit. Switch it to inactive if so.
	if (m_cherry->Intersect(entity))
	{
		m_cherry->m_isActive = false;
		return true;
	}
	else
	{
		return false;
	}
}

void World::SetCherryActive(bool isActive)
{
	m_cherry->m_isActive = isActive;
}

void World::GetPath(
	const MovableEntity& entity,
	int toX, int toY,
	MoveDirection& targetDirection) const
{
	// Grab tiles.
	const WorldTile* fromTile = GetTile(entity.GetCurrentTileX(), entity.GetCurrentTileY());
	assert(fromTile != NULL);
	Vector2f toPos(
		(float)(toX*TILE_SIZE) + (float)TILE_SIZE/2.f,
		(float)(toY*TILE_SIZE) + (float)TILE_SIZE/2.f);

	// Call internal path-finding code.
	Pathfind(entity, fromTile, &toPos, targetDirection);
}

void World::GetRandomPath(
	const MovableEntity& entity,
	MoveDirection& targetDirection) const
{
	// Grab origin tile.
	const WorldTile* fromTile = GetTile(entity.GetCurrentTileX(), entity.GetCurrentTileY());
	assert(fromTile != NULL);

	// Call internal path-finding code.
	Pathfind(entity, fromTile, NULL, targetDirection);
}

// Convert a movement X/Y into a MoveDirection enum value.
World::MoveDirection World::DetermineMoveDirection(int moveX, int moveY)
{
	if (moveX < 0)
		return LEFT;
	else if (moveX > 0)
		return RIGHT;
	else if (moveY < 0)
		return UP;
	else
		return DOWN;
}

const WorldTile* World::GetTile(int fromX, int fromY, bool clampInBounds) const
{
	// Check out of bounds. Otherwise look up directly.
	// We only correct for out-of-bounds coordinates if explicitly specified
	// (e.g. ghost scatter mode).
	if (clampInBounds)
	{
		if (fromX < 0)
			fromX = 0;
		else if (fromX >= m_sizeX)
			fromX = m_sizeX-1;
		if (fromY < 0)
			fromY = 0;
		else if (fromY >= m_sizeY)
			fromY = m_sizeY-1;
	}
	else if (fromX < 0 || fromX >= m_sizeX || fromY < 0 || fromY >= m_sizeY)
		return NULL;

	return m_worldTiles[fromY*m_sizeX + fromX];
}

void World::AddTileIfValid(
	const MovableEntity& entity,
	int xCoord, int yCoord,
	std::vector<const WorldTile*>& neighbours) const
{
	// This isn't as pretty as it could be. See note in comment above Pathfind method declaration.
	if (entity.CanMoveToTile(xCoord, yCoord, *this) )
		neighbours.push_back(GetTile(xCoord, yCoord));
}

void World::Pathfind(
	const MovableEntity& entity,
	const WorldTile* fromTile,
	const Vector2f* toPos,
	MoveDirection& targetDirection) const
{
	// NOTE: This simple method may not work so well for different maze shapes.
	// In that case, the below could be replaced by a more general iterative breadth-first
	// search, which should still be faster (and more correct) than the original code.
	// That is an unnecessarily complex solution just to recreate classic Pac-Man though.
	assert(fromTile != NULL);
	Vector2f aFromPos = fromTile->GetCentre();

	// Construct a vector of all passable neighbour tiles.
	std::vector<const WorldTile*> neighbours;

	AddTileIfValid(entity, fromTile->m_xCoord, fromTile->m_yCoord - 1, neighbours); // UP
	AddTileIfValid(entity, fromTile->m_xCoord, fromTile->m_yCoord + 1, neighbours); // DOWN
	AddTileIfValid(entity, fromTile->m_xCoord + 1, fromTile->m_yCoord, neighbours); // RIGHT
	AddTileIfValid(entity, fromTile->m_xCoord - 1, fromTile->m_yCoord, neighbours); // LEFT

	// Couple of minor optimisations to skip the below if unnecessary.
	const WorldTile* tile = NULL;
	if (neighbours.empty())
		return; // No valid direction, so don't set.
	else if (neighbours.size() == 1)
		tile = neighbours[0]; // Only one to pick.
	// If no destination, we just move randomly. So pick a random (but still
	// valid) tile to move onto. This isn't ideal randomness, but should be
	// just fine to pick one of up to 4 directions.
	else if (toPos == NULL)
		tile = neighbours[rand() % neighbours.size()];

	// Bail early if we already picked a tile
	if (tile != NULL)
	{
		targetDirection = DetermineMoveDirection(
			tile->m_xCoord - fromTile->m_xCoord,
			tile->m_yCoord - fromTile->m_yCoord);
		return;
	}

	// Otherwise, do proper pathing: pick the best tile from our candidate non-blocking neighbours.
	float distSq;
	float bestDistSq = -1.f;
	MoveDirection candidateDir;
	for (std::vector<const WorldTile*>::const_iterator it = neighbours.begin(); it != neighbours.end(); it++)
	{
		tile = *it;
		// Direction from current tile to candidate tile
		candidateDir = DetermineMoveDirection(
			tile->m_xCoord - fromTile->m_xCoord,
			tile->m_yCoord - fromTile->m_yCoord);
		// Distance from candidate tile to target position.
		distSq = (tile->GetCentre() - *toPos).LengthSq();
		// We choose this tile if either:
		//  1) The tile is is closer to the target, 
		//  2) The tile is the same distance but has a direction with higher precedence (using MoveDirection enum val) 
		if ( (bestDistSq < 0.f || distSq < bestDistSq) ||
			 (distSq == bestDistSq && candidateDir < targetDirection) )
		{
			bestDistSq = distSq;
			targetDirection = candidateDir;
		}
	}
}
