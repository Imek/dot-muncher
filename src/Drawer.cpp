#include "Drawer.h"
#include "SDL.h"
#include "SDL_image.h"
#include "SDL_ttf.h"

static const std::string IMAGES_PATH = "data/images/";
static const std::string FONTS_PATH = "data/freefont-ttf/sfd/";

Drawer* Drawer::Create(SDL_Surface* window)
{
	Drawer* drawer = new Drawer(window);

	// Try to init, checking for failure.
	if (!drawer->Init())
	{
		delete drawer;
		drawer = NULL;
	}

	return drawer;
}

Drawer::Drawer(SDL_Surface* window)
: m_window(window)
{
}

Drawer::~Drawer(void)
{
	// Free all of our managed memory.
	for (SurfacePtrVec::iterator iter = m_loadedSurfaces.begin(); iter != m_loadedSurfaces.end(); iter++)
		SDL_FreeSurface(*iter);
	for (RectPtrVec::iterator iter = m_sizeRects.begin(); iter != m_sizeRects.end(); iter++)
		delete *iter;
	for (RectPtrVec::iterator iter = m_posRects.begin(); iter != m_posRects.end(); iter++)
		delete *iter;
	for (FontPtrVec::iterator iter = m_loadedFonts.begin(); iter != m_loadedFonts.end(); iter++)
		TTF_CloseFont(*iter);
}

bool Drawer::Init()
{
	if (!m_window)
		return false;

	return true;
}

int Drawer::LoadImage(const char* imageFile)
{
	std::string imagePath(IMAGES_PATH);
	imagePath += imageFile;
	SDL_Surface* surface = IMG_Load(imagePath.c_str());

	if (!surface)
		return -1;

	// Optimisation of loaded surface (see SDL docs).
	// It's copied, so the original surface can be freed immediately.
	SDL_Surface* optimizedSurface = SDL_DisplayFormatAlpha(surface);
	SDL_FreeSurface(surface);
	
	SDL_Rect* sizeRect = new SDL_Rect;
	sizeRect->x = 0;
	sizeRect->y = 0;
	sizeRect->w = optimizedSurface->w;
	sizeRect->h = optimizedSurface->h;

	SDL_Rect* posRect = new SDL_Rect;
	posRect->x = 0;
	posRect->y = 0;

	// Store them for use by Draw.
	int nextID = m_loadedSurfaces.size();
	assert(nextID == m_sizeRects.size() && nextID == m_posRects.size());
	m_loadedSurfaces.push_back(optimizedSurface);
	m_sizeRects.push_back(sizeRect);
	m_posRects.push_back(posRect);

	return nextID;
}

int Drawer::LoadFont(const char* fontFile)
{
	// Simply load the font from the file, and if successful store and return the ID.
	// -1 indicates a valid font could not be loaded.
	std::string fontPath(FONTS_PATH);
	fontPath += fontFile;
	TTF_Font* font = TTF_OpenFont(fontPath.c_str(), 20);
	if (!font)
		return -1;

	int nextID = m_loadedSurfaces.size();
	m_loadedFonts.push_back(font);
	return nextID;
}

void Drawer::Draw(int imageID, int xPos, int yPos)
{
	// We were passed an ID that refers to an index in m_loadedSurfaces. Quick validity check.
	// (we could be paranoid and also check m_sizeRects etc, but if those were mismatched something
	// already went badly wrong in init)
	if (imageID < 0 || imageID > (int)m_loadedSurfaces.size())
		return; // Invalid ID passed.

	SDL_Surface* optimizedSurface = m_loadedSurfaces[imageID];
	SDL_Rect* sizeRect = m_sizeRects[imageID];
	SDL_Rect* posRect = m_posRects[imageID];

	if (!optimizedSurface || !sizeRect || !posRect)
		return; // Looks like this ID was "valid" but improperly initialised.

	posRect->x = xPos;
	posRect->y = yPos;
	SDL_BlitSurface(optimizedSurface, sizeRect, m_window, posRect);
}

void Drawer::DrawText(const char* text, int fontID, int xPos, int yPos)
{
	// Simple validation of ID then look up the font, as we do in Draw.
	if (fontID < 0 || fontID > (int)m_loadedFonts.size())
		return;
	
	// XXX: Currently we only cache the font itself. Could cache the other data too as we do in
	// Draw, but it is more complicated for text that can vary frequently as with scores.
	TTF_Font* font = m_loadedFonts[fontID];

	// Fixed hard-coded colour for the moment..
	SDL_Color fg = {0, 255, 255, 255};
	SDL_Surface* surface = TTF_RenderText_Solid(font, text, fg);

	SDL_Surface* optimizedSurface = SDL_DisplayFormat(surface);

	SDL_Rect sizeRect;
	sizeRect.x = 0;
	sizeRect.y = 0;
	sizeRect.w = optimizedSurface->w;
	sizeRect.h = optimizedSurface->h;

	SDL_Rect posRect;
	posRect.x = xPos;
	posRect.y = yPos;

	SDL_BlitSurface(optimizedSurface, &sizeRect, m_window, &posRect) ;

	SDL_FreeSurface(optimizedSurface);
	SDL_FreeSurface(surface);
}
