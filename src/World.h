#ifndef WORLD_H
#define WORLD_H

#include <list>
#include <vector>
#include "Vector2f.h"

class Drawer;
class WorldTile;
class Entity;
class MovableEntity;


// Class to manage the game world tile grid and static objects within it.
class World
{
public:
	World(void);
	~World(void);

	// NOTE: These (particularly coordinates) could be made configurable in map.txt if needed.
	static const int TILE_SIZE = 22;
	static const int AVATAR_HOME_X = 13;
	static const int AVATAR_HOME_Y = 22;
	static const int GHOST_HOME_X = 13;
	static const int GHOST_HOME_Y = 13;
	static const int GHOST_EXIT_X = 13;
	static const int GHOST_EXIT_Y = 10;

	// Enumeration for move directions.
	// Used to index into the avatar entity's image array for directions.
	// Note that we also use this as an order of precedence for ghost pathfinding.
	enum MoveDirection { UP, LEFT, DOWN, RIGHT, MD_MAX };

	// Convert a movement X/Y into a MoveDirection enum value.
	static MoveDirection DetermineMoveDirection(int moveX, int moveY);

	// Initialise function, given a Drawer.
	bool Init(Drawer& drawer);

	void Draw(Drawer& drawer) const;

	// Getters to determine attributes of a tile at given coordinates.
	bool TileIsInBounds(int xCoord, int yCoord) const;
	bool TileIsValid(int xCoord, int yCoord) const;
	bool TileIsTunnel(int xCoord, int yCoord) const;
	bool TileIsGhostHouse(int xCoord, int yCoord) const;

	// Intersection checks for the avatar. Will also remove the respective object.
	bool CheckIntersectedDot(const Entity& entity);
	bool CheckIntersectedBigDot(const Entity& entity);
	bool CheckIntersectedCherry(const Entity& entity);

	// Toggle the cherry's active state.
	void SetCherryActive(bool isActive);

	// Getters for world dimensions.
	int GetSizeX() const { return m_sizeX; }
	int GetSizeY() const { return m_sizeY; }

	// Return the total initial number of dots, both big and small, on this level.
	int GetNumDots() const { return m_numDots; }

	// Given an entity and destination coordinates, see if we can get a path
	// to the given target. If we can, set by reference the target direction.
	void GetPath(
		const MovableEntity& entity,
		int toX, int toY,
		MoveDirection& targetDirection) const;
	// If we can, set a random but valid move direction for the given entity.
	void GetRandomPath(
		const MovableEntity& entity,
		MoveDirection& targetDirection) const;

private:
	// Private dummy copy/assignment operators for safety, since we're managing memory; we don't
	// want or need this functionality in this class.
	World(const World&);
	World& operator=(const World&);

	const WorldTile* GetTile(int fromX, int fromY, bool clampInBounds = false) const;
	
	// Helper function for Pathfind to add a candidate neighbour tile.
	void AddTileIfValid(
		const MovableEntity& entity,
		int xCoord, int yCoord,
		std::vector<const WorldTile*>& neighbours) const;
	// Given an entity, an origin tile and a destination position, find a path as a
	// sequence of tiles, setting by reference the target direction if we can.
	// The destination position can be omitted (i.e. NULL), in which case we move randomly.
	// XXX: The circular referencing between World code and MovableEntity code here could be considered
	// a bit messy. But doing it this way means better separation/encapsulation of path code in World and
	// game entity logic in MovableEntity and its subclasses. Alternative might be to separate out
	// pathing code completely.
	void Pathfind(
		const MovableEntity& entity,
		const WorldTile* fromTile, 
		const Vector2f* toPos,
		MoveDirection& targetDirection) const;

	// Does first read through map to determine its size and validate that it's well-formed.
	// Also initialises m_worldTiles at the correct size.
	bool InitMapSize();

	// Reads through the map file and initialises all tile elements (pathmap tiles and entities).
	bool InitMapObjects();

	// Functions for initialising individual map tile elements.
	void InitWorldTile(int xCoord, int yCoord, const char& tileChar);
	void InitDot(int xCoord, int yCoord);
	void InitBigDot(int xCoord, int yCoord);
	void InitCherry(int xCoord, int yCoord);

	// Store image IDs, initially -1 until loaded
	int m_mazeImageID;
	int m_dotImageID;
	int m_bigDotImageID;
	int m_cherryImageID;

	// Size values, determined on init
	// NOTE: Keep these as signed ints just for consistency throughout, as negative values may
	// be passed around (to check wrapping/adjacency or indicate no value)
	int m_sizeX;
	int m_sizeY;
	int m_numDots;

	// Tile map is a fixed-size array (row-by-row), determined on InitMapSize.
	WorldTile** m_worldTiles;

	// Entities managed by this class.
	std::list<Entity*> m_dots;
	std::list<Entity*> m_bigDots;
	Entity* m_cherry;

};

#endif // WORLD_H
