#include <assert.h>
#include "MovableEntity.h"

MovableEntity::MovableEntity(
	int xCoord,
	int yCoord,
	int imageID,
	float speed,
	int aInitialMovementX,
	int aInitialMovementY)
: Entity((float)xCoord, (float)yCoord, imageID)
, m_speed(speed)
, m_homeTileX(xCoord), m_homeTileY(yCoord)
, m_currentTileX(xCoord), m_currentTileY(yCoord)
, m_nextTileX(xCoord), m_nextTileY(yCoord)
, m_currentMovementX(aInitialMovementX), m_currentMovementY(aInitialMovementY)
, m_desiredMovementX(0), m_desiredMovementY(0)
{
}

MovableEntity::~MovableEntity(void)
{
}

void MovableEntity::SetPosition(int toX, int toY)
{
	Entity::SetPosition(toX, toY);
	m_currentTileX = toX;
	m_currentTileY = toY;
}

void MovableEntity::Update(float timeSecs, const World& world)
{
	// Update tile for the next update.
	if (IsAtDestination())
		UpdateNextTile(timeSecs, world);
	// If still moving to a destination, update movement. Note that we re-check IsAtDestination,
	// since UpdateNextTile may have updated our destination.
	if (!IsAtDestination())
		UpdateMovement(timeSecs);

}

World::MoveDirection MovableEntity::GetMovingDirection() const
{
	return World::DetermineMoveDirection(m_currentMovementX, m_currentMovementY);
}

World::MoveDirection MovableEntity::GetDesiredMovingDirection() const
{
	return World::DetermineMoveDirection(m_desiredMovementX, m_desiredMovementY);
}

void MovableEntity::SetDesiredMovingDirection(World::MoveDirection direction)
{
	switch (direction)
	{
	case World::UP:
		m_desiredMovementX = 0;
		m_desiredMovementY = -1;
		break;
	case World::RIGHT:
		m_desiredMovementX = 1;
		m_desiredMovementY = 0;
		break;
	case World::DOWN:
		m_desiredMovementX = 0;
		m_desiredMovementY = 1;
		break;
	case World::LEFT:
		m_desiredMovementX = -1;
		m_desiredMovementY = 0;
		break;
	}
}

void MovableEntity::SetDesiredMovingDirection(int desiredMovementX, int desiredMovementY)
{
	assert(abs(desiredMovementX) + abs(desiredMovementY) == 1);
	m_desiredMovementX = desiredMovementX;
	m_desiredMovementY = desiredMovementY;
}

bool MovableEntity::IsAtDestination() const
{
	return (m_currentTileX == m_nextTileX && m_currentTileY == m_nextTileY);
}

bool MovableEntity::IsMoving() const
{
	return (m_currentMovementX != 0 || m_currentMovementY != 0);
}

bool MovableEntity::CanMoveToTile(int xCoord, int yCoord, const World& world) const
{
	// Base behaviour - always check if not blocking and valid.
	return world.TileIsValid(xCoord, yCoord);
}

void MovableEntity::Reset()
{
	SetPosition(m_homeTileX, m_homeTileY);
	m_currentTileX = m_nextTileX = m_homeTileX;
	m_currentTileY = m_nextTileY = m_homeTileY;

	SetDesiredMovingDirection(World::LEFT);
}

void MovableEntity::UpdateNextTile(float timeSecs, const World& world)
{
	bool doMove;
	int desiredTileX = m_currentTileX + m_desiredMovementX;
	int desiredTileY = m_currentTileY + m_desiredMovementY;
	// Switch to our queued direction if there's a corner we can turn and still have a valid move.
	if ((desiredTileX != m_currentTileX || desiredTileY != m_currentTileY) &&
		CanMoveToTile(desiredTileX, desiredTileY, world))
	{
		m_currentMovementX = m_desiredMovementX;
		m_currentMovementY = m_desiredMovementY;
		m_desiredMovementX = 0;
		m_desiredMovementY = 0;
		doMove = true;
	}
	// Otherwise see if we can keep moving in the same direction.
	else
	{
		// Determine our next desired tile for movement.
		desiredTileX = m_currentTileX + m_currentMovementX;
		desiredTileY = m_currentTileY + m_currentMovementY;
		// Special case: wrapping around in a tunnel. In this case we teleport to the opposite
		// tile, and our next movement becomes from that tile.
		if (world.TileIsTunnel(m_currentTileX, m_currentTileY) && !world.TileIsInBounds(desiredTileX, desiredTileY))
		{
			// Wrap our position to the opposite side of the world.
			// NOTE: More general would be to do a proper modulo (where -1 % 30 -> 29)
			// with a maths helper or library as available. Below solution, though verbose,
			// is sufficient for us, since our movement is always a unit vector.
			if (desiredTileX < 0)
				desiredTileX = world.GetSizeX() + desiredTileX;
			else
				desiredTileX %= world.GetSizeX();
			if (desiredTileY < 0)
				desiredTileY = world.GetSizeY() + desiredTileY;
			else
				desiredTileY %= world.GetSizeY();
			// Wrapped movement is only valid if both our arrival tile and the following one are valid.
			if (CanMoveToTile(desiredTileX, desiredTileY, world))
				SetPosition(desiredTileX, desiredTileY);
			desiredTileX += m_currentMovementX;
			desiredTileY += m_currentMovementY;
		}
		// Either way we must check the desired movement tile is valid before moving.
		doMove = CanMoveToTile(desiredTileX, desiredTileY, world);
	}

	// We want to update the next tile if it's valid to move. Otherwise stop.
	if (doMove)
	{
		m_nextTileX = desiredTileX;
		m_nextTileY = desiredTileY;
	}
	else
	{
		m_currentMovementX = 0;
		m_currentMovementY = 0;
	}
}

void MovableEntity::UpdateMovement(float timeSecs)
{
	// TODO: Optimise by ensuring that movement vector is a unit,
	// so we don't have to do this costly normalisation.
	Vector2f destination(m_nextTileX*World::TILE_SIZE, m_nextTileY*World::TILE_SIZE);
	Vector2f direction = destination - m_position;

	float distanceToMove = timeSecs * m_speed;

	if (distanceToMove > direction.Length())
	{
		m_position = destination;
		m_currentTileX = m_nextTileX;
		m_currentTileY = m_nextTileY;
	}
	else
	{
		direction.Normalise();
		m_position += direction * distanceToMove;
	}
}
