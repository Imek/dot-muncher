#ifndef PACBRO_H
#define PACBRO_H

#include "Vector2f.h"

struct SDL_Surface;
class Drawer;
class Avatar;
class World;
class GhostManager;

// Central class that manages the DotMuncher game initialisation, updating and drawing.
// NOTE: A potential refactor would be to create a base GameComponent class with
// and interface for initialisation, updating (given time & state) and drawing. This
// requires a bit of refactor of how the game state is represented, since some code
// needs specific objects like the Avatar or other Ghosts.
class DotMuncher
{
public:
	static DotMuncher* Create(Drawer* drawer);
	~DotMuncher(void);

	bool Update(float timeSecs);
	bool Draw();

private:
	DotMuncher(Drawer* drawer);
	// Private dummy copy/assignment operators for safety, since we're managing memory; we don't
	// want or need this functionality in this class.
	DotMuncher(const DotMuncher&);
	DotMuncher& operator=(const DotMuncher&);

	// First-time game initialisation. Creates entities and loads assets, returning success flag.
	bool Init();
	// Update our constrol state from player input.
	bool UpdateInput();
	// Perform update logic for intersection between game entities.
	void UpdateIntersection();

	// Checks whether the player has won the game, and sets m_gameOverText if so.
	bool CheckGameOver();

	Drawer* m_drawer;
	int m_fontID;
	// String set when the player has lost or the game over condition has been satisfied.
	char* m_gameOverText;

	float m_timeToNextUpdate;
	// Counter to count down the lifetime of the bonus fruit (cherry) when it is activated.
	float m_bonusCounter;

	// Game state values.
	int m_lives;
	int m_score;
	int m_numDotsCollected;
	int m_ghostKillMultiplier;
	int m_FPS;

	// Pointers to our objects, created in Init..
	Avatar* m_avatar;
	GhostManager* m_ghostManager;
	World* m_world;
};

#endif // PACBRO_H
