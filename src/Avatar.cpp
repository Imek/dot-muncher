#include "Avatar.h"

#include "Drawer.h"

Avatar::Avatar(int xCoord, int yCoord, Drawer& drawer)
: MovableEntity(xCoord, yCoord, -1, 180.0f, -1, 0)
, m_lastImageIndex(0)
, m_timeSpentMoving(0.0f)
{
	// Load our images, placing them in the correct direction indexes for easy look-up in Update.
	m_openImageIDs[World::UP] = drawer.LoadImage("open_up_32.png");
	m_openImageIDs[World::RIGHT] = drawer.LoadImage("open_right_32.png");
	m_openImageIDs[World::DOWN] = drawer.LoadImage("open_down_32.png");
	m_openImageIDs[World::LEFT] = drawer.LoadImage("open_left_32.png");

	m_closedImageIDs[World::UP] = drawer.LoadImage("closed_up_32.png");
	m_closedImageIDs[World::RIGHT] = drawer.LoadImage("closed_right_32.png");
	m_closedImageIDs[World::DOWN] = drawer.LoadImage("closed_down_32.png");
	m_closedImageIDs[World::LEFT] = drawer.LoadImage("closed_left_32.png");
}

Avatar::~Avatar(void)
{
}

void Avatar::Update(float timeSecs, const World& world)
{
	// Update the image to display based on whether we're still moving.
	if (IsMoving())
	{
		// Call movable code to get the direction value that corresponds to an image index (0-3)
		m_lastImageIndex = GetMovingDirection();

		// Determine whether mouth should be open or closed.
		m_timeSpentMoving += timeSecs;
		if (m_timeSpentMoving > 0.15f)
			m_imageID = m_closedImageIDs[m_lastImageIndex];
		else
			m_imageID = m_openImageIDs[m_lastImageIndex];
		// Reset to beginning after one cycle
		if (m_timeSpentMoving > 0.3f)
			m_timeSpentMoving = 0.0f;
	}
	else
	{
		m_timeSpentMoving = 0.0f;
		m_imageID = m_closedImageIDs[m_lastImageIndex];
	}

	// Common movable code handles movement.
	MovableEntity::Update(timeSecs, world);

}

bool Avatar::CanMoveToTile(int xCoord, int yCoord, const World& world) const
{
	// Only rule for the avatar is that the tile is passable and that it's not in the ghost house.
	if (!MovableEntity::CanMoveToTile(xCoord, yCoord, world))
		return false;
	else
		return !world.TileIsGhostHouse(xCoord, yCoord);
}
