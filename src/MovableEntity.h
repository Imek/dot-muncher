#ifndef MOVABLEENTITY_H
#define MOVABLEENTITY_H

#include "Entity.h"
#include "Vector2f.h"
#include "World.h"

// Sub-class of Entity for entities that can be moved dynamically.
// These entities keep track of their tiles and, if moving, the next tile they will land on.
class MovableEntity : public Entity
{
public:
	// Overridden constructor which also takes a speed.
	// Note also that although Entity accepts float coordinates, we expect
	// movable entities to be exactly on a tile and thus take integers.
	MovableEntity(
		int xCoord,
		int yCoord,
		int imageID,
		float speed = 0.0f,
		int aInitialMovementX = 0,
		int aInitialMovementY = 0);
	~MovableEntity(void);

	// Override SetPosition, as some movable-specific values must be updated.
	void SetPosition(int toX, int toY);

	virtual void Update(float timeSecs, const World& world);

	// Getters for current X/Y tile coordinates.
	int GetCurrentTileX() const { return m_currentTileX; }
	int GetCurrentTileY() const { return m_currentTileY; }
	// Getters for movement values.
	int GetCurrentMovementX() const { return m_currentMovementX; }
	int GetCurrentMovementY() const { return m_currentMovementY; }

	// Determine current movement direction as a MoveDirection enum value.
	// Value is only valid if currently moving (not at destination).
	World::MoveDirection GetMovingDirection() const;
	// Determine desired movement direction based on last set.
	World::MoveDirection GetDesiredMovingDirection() const;

	// Set the next desired movement direction.
	void SetDesiredMovingDirection(World::MoveDirection direction);
	void SetDesiredMovingDirection(int desiredMovementX, int desiredMovementY);

	// Determine whether now we've arrived at the "next" tile in movement.
	bool IsAtDestination() const;
	// Determine whether this entity is still moving.
	bool IsMoving() const;

	// Determine whether this entity is able/willing to move on to a given path tile.
	// This is for purposes of blocking (e.g. player can't go in the ghost house,
	// ghosts have certain restrictions to movement, etc)
	virtual bool CanMoveToTile(int xCoord, int yCoord, const World& world) const;

	// Reset this entity's position and movement state to its starting values.
	virtual void Reset();

protected:
	// Current moving speed, if moving.
	float m_speed;

private:
	// XXX: Conceptually these could be vectors, but we don't need floats
	// or any involved vector maths. If we had a Vector2d we could use that though.

	// Update navigation, setting the next tile as appropriate.
	void UpdateNextTile(float timeSecs, const World& world);
	// Update the actual movement of the entity, if moving.
	void UpdateMovement(float timeSecs);

	// Initial home location tile x/y coordinates, for resetting.
	int m_homeTileX;
	int m_homeTileY;

	// Current location tile x/y coordinates
	int m_currentTileX;
	int m_currentTileY;

	// Movement direction and stored next tile coordinates, if currently moving.
	// Movement should be 0 and next tile should match current if we're stopped.
	int m_nextTileX;
	int m_nextTileY;
	int m_currentMovementX;
	int m_currentMovementY;

	// Desired movement direction next time we reach an intersection (or want to reverse).
	int m_desiredMovementX;
	int m_desiredMovementY;
};

#endif // MOVABLEENTITY_H
