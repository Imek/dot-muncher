#include "SDL.h"
#include "time.h"
#include "assert.h"
#include "DotMuncher.h"
#include "Drawer.h"
#include "SDL_ttf.h"
#include "SDL_image.h"
#include <iostream>

int main(int argc, char **argv)
{
	// SDL Initialisation
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		assert(0 && "Failed to initialise video!");
		exit(-1);
	}
	
	SDL_Surface* window = SDL_SetVideoMode(1024, 768, 32, SDL_DOUBLEBUF | SDL_HWSURFACE);
	if (!window)
	{
		assert(0 && "Failed to create window!");
		exit(-1);
	}

	IMG_Init(IMG_INIT_JPG|IMG_INIT_PNG);

	if (TTF_Init() == -1)
	{
		assert(0 && "Failed to create ttf!");
		exit(-1);
	}

	SDL_WM_SetCaption("Dot Muncher", "");

	// Initialise the random seed
	srand(static_cast<unsigned>(time(0)));

	// Initialise Drawer instance that we will use to draw the game state
	Drawer* drawer = Drawer::Create(window);
	if (!drawer)
	{
		assert(0 && "Drawer init failed! SDL window problem?");
		exit(-1);
	}

	// Main game initialisation.
	DotMuncher* dotmuncher = DotMuncher::Create(drawer);
	if (!dotmuncher)
	{
		assert(0 && "Game init failed! Is map.txt valid?");
		exit(-1);
	}
	
	// Main game loop. Runs until we get a quit condition.
	// TODO: All this into a reusable class.
	float currentFrame;
	float elapsedTime;
	float lastFrame = (float) SDL_GetTicks() * 0.001f;
	SDL_Event event;
	while (SDL_PollEvent(&event) >= 0 && event.type != SDL_QUIT)
	{
		currentFrame = (float) SDL_GetTicks() * 0.001f;
		elapsedTime = currentFrame - lastFrame;

		// Update game state based on time passed since last update. Returns false if it's the end.
		if (!dotmuncher->Update(elapsedTime))
			break;

		// Draw game state for this frame
		dotmuncher->Draw();
		
		lastFrame = currentFrame;
		SDL_Flip(window);
		SDL_Delay(1);
	}

	// Main loop finished; perform clean-up.
	delete dotmuncher;
	delete drawer;

	TTF_Quit();
	IMG_Quit();
	SDL_Quit();

	return 0;
}
