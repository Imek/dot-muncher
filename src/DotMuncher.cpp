#include "DotMuncher.h"
#include "Drawer.h"
#include "SDL.h"
#include <math.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>

#include "Avatar.h"
#include "World.h"
#include "GhostManager.h"

// Some gameplay settings, just to make tweaking easier. These could be
// made configurable if we had multiple levels or difficulties.
static const float BONUS_TIME_BASE = 9.0f;
static const float BONUS_TIME_MAXADD = 1.0f;


DotMuncher* DotMuncher::Create(Drawer* drawer)
{
	DotMuncher* dotmuncher = new DotMuncher(drawer);

	if (!dotmuncher->Init())
	{
		delete dotmuncher;
		dotmuncher = NULL;
	}

	return dotmuncher;
}

DotMuncher::DotMuncher(Drawer* drawer)
: m_drawer(drawer)
, m_fontID(-1)
, m_gameOverText(NULL)
, m_timeToNextUpdate(0.f)
, m_score(0)
, m_ghostKillMultiplier(1)
, m_numDotsCollected(0)
, m_FPS(0)
, m_lives(3)
// These are constructed on Init
, m_avatar(NULL)
, m_ghostManager(NULL)
, m_world(NULL)
{
}

DotMuncher::~DotMuncher(void)
{
	delete m_avatar;
	delete m_ghostManager;
	delete m_world;
}

bool DotMuncher::Init()
{
	// Load the common font that we use for all UI text.
	m_fontID = m_drawer->LoadFont("FreeMono.ttf");
	// Load the game entities, as well as relevant images.
	m_avatar = new Avatar(World::AVATAR_HOME_X, World::AVATAR_HOME_Y, *m_drawer);

	// Create the world, which manages the maze tiles and static entities.
	m_world = new World();

	// World init sets up and does image loading for its own entities.
	// Returns false if it failed to read and initialise the map.
	if (!m_world->Init(*m_drawer))
		return false;

	// Ghost manager sets up the ghosts and will manage related game logic.
	m_ghostManager = new GhostManager();
	if (!m_ghostManager->Init(*m_drawer, *m_world))
		return false;

	// Everything is cool if we fell through.
	return true;
}

bool DotMuncher::Update(float timeSecs)
{
	// EARLY RETURNS

	// Returns false if quit is requested.
	if (!UpdateInput())
		return false;

	// If game is over, we stop updating state.
	if (CheckGameOver())
		return true;


	// TIMER CODE

	// Cherry should time out if not grabbed.
	if (m_bonusCounter > 0.f)
	{
		m_bonusCounter -= timeSecs;
		if (m_bonusCounter <= 0.f)
			m_world->SetCherryActive(false);
	}


	// GAME COMPONENT UPDATE CALLS
	m_avatar->Update(timeSecs, *m_world);
	m_ghostManager->Update(timeSecs, *m_world, *m_avatar);


	// INTERSECTION CHECKS
	UpdateIntersection();


	// UPDATE FPS
	
	if (timeSecs > 0)
		m_FPS = (int) (1 / timeSecs);

	return true;
}



bool DotMuncher::UpdateInput()
{
	// Take keyboard state from SDL and convert to our direction value.
	Uint8 *keystate = SDL_GetKeyState(NULL);

	if (keystate[SDLK_UP])
		m_avatar->SetDesiredMovingDirection(World::UP);
	else if (keystate[SDLK_DOWN])
		m_avatar->SetDesiredMovingDirection(World::DOWN);
	else if (keystate[SDLK_RIGHT])
		m_avatar->SetDesiredMovingDirection(World::RIGHT);
	else if (keystate[SDLK_LEFT])
		m_avatar->SetDesiredMovingDirection(World::LEFT);

	// Return flag indicates quit.
	if (keystate[SDLK_ESCAPE])
		return false;

	return true;
}

void DotMuncher::UpdateIntersection()
{
	// Check the avatar has collided with ghosts. Do this first, since player death
	// may render it unnecessary to check anything else.
	Ghost* thisGhost;
	const GhostVec& ghosts = m_ghostManager->GetGhosts();
	for (GhostVec::const_iterator it = ghosts.begin(); it != ghosts.end(); it++)
	{
		thisGhost = *it;
		if (thisGhost->m_curState != Ghost::DEAD && (thisGhost->Intersect(*m_avatar, 400.f)))
		{
			// Case A: ghost is active, so avatar dies.
			if (thisGhost->m_curState == Ghost::ACTIVE)
			{
				m_lives--;
				// Reset state, unless game over.
				if (!CheckGameOver())
				{
					// Reset timed elements.
					m_bonusCounter = 0.f;
					m_world->SetCherryActive(false);

					// Reset avatar to game start state (position and movement).
					m_avatar->Reset();
					// Reset all ghost-related data too.
					m_ghostManager->Reset();
				}
				break; // Don't want to check anything else if player died!
			}
			// Case B: ghost is claimable, so ghost dies.
			else
			{
				assert(thisGhost->m_curState == Ghost::FRIGHTENED);
				m_score += 200 * m_ghostKillMultiplier;
				m_ghostKillMultiplier *= 2;
				thisGhost->Die();
			}
		}
	}

	// Collect dots and activate any behaviour that depends on dot collection.
	if (m_world->CheckIntersectedDot(*m_avatar))
	{
		m_score += 10;
		m_numDotsCollected++;
		if (m_numDotsCollected == 70 || m_numDotsCollected == 170)
		{
			m_world->SetCherryActive(true);
			// Bonus fruit lasts between 9 and 10 seconds. I know using C rand is not perfect, but
			// it's sufficient for this purpose.
			m_bonusCounter = BONUS_TIME_BASE + ((float)rand() / (float)RAND_MAX) * BONUS_TIME_MAXADD;
		}
	}
	// Collect cherry.
	if (m_world->CheckIntersectedCherry(*m_avatar))
	{
		m_score += 100;
		m_world->SetCherryActive(false);
	}
	// Collect big dot and activate relevant behaviour.
	if (m_world->CheckIntersectedBigDot(*m_avatar))
	{
		m_score += 20;
		m_numDotsCollected++;
		// Note that the multiplier always resets when you collect a big dot, even if a previous
		// timer was still going.
		m_ghostKillMultiplier = 1;
		m_ghostManager->OnEatenBigDot();
	}
}

bool DotMuncher::CheckGameOver()
{
	// Already determined game over.
	if (m_gameOverText != NULL)
	{
		return true;
	}
	// Player loses if they run out of lives.
	else if (m_lives <= 0)
	{
		m_gameOverText = "You lose!";
		return true;
	}
	// To win, the player must simply collect all dots.
	else if (m_numDotsCollected >= m_world->GetNumDots())
	{
		m_gameOverText = "You win!";
		return true;
	}
	else
	{
		return false;
	}
}

bool DotMuncher::Draw()
{
	// Call Draw on all game subsystems.
	m_world->Draw(*m_drawer);
	m_avatar->Draw(*m_drawer);
	m_ghostManager->Draw(*m_drawer);

	// Display score text
	std::string scoreString;
	std::stringstream scoreStream;
	scoreStream << m_score;
	scoreString = scoreStream.str();
	m_drawer->DrawText("Score", m_fontID, 20, 50);
	m_drawer->DrawText(scoreString.c_str(), m_fontID, 90, 50);

	// If Update has set our game over text, display it
	// As we don't yet have a fancy game over screen, just display in place of the lives counter.
	if (m_gameOverText != NULL)
	{
		m_drawer->DrawText(m_gameOverText, m_fontID, 20, 80);
	}
	else
	{
		std::string livesString;
		std::stringstream liveStream;
		liveStream << m_lives;
		livesString = liveStream.str();
		m_drawer->DrawText("Lives", m_fontID, 20, 80);
		m_drawer->DrawText(livesString.c_str(), m_fontID, 90, 80);
	}

	// Draw FPS counter
	m_drawer->DrawText("FPS", m_fontID, 880, 50);
	std::string fpsString;
	std::stringstream fpsStream;
	fpsStream << m_FPS;
	fpsString = fpsStream.str();
	m_drawer->DrawText(fpsString.c_str(), m_fontID, 930, 50);

	return true;
}
