#ifndef WORLDTILE_H
#define WORLDTILE_H

#include "Vector2f.h"

// Represents a tile in the game world. Created by reading from MAP_FILE - see World.cpp.
class WorldTile
{
public:
	WorldTile(int xCoord, int yCoord, bool isBlocking, bool isTunnel, bool isGhostHouse);
	~WorldTile(void);

	// Get position vector of centre of tile.
	Vector2f WorldTile::GetCentre() const;

	// A tile's fixed member attributes.
	const int m_xCoord;
	const int m_yCoord;
	const bool m_isBlocking;
	const bool m_isTunnel;
	const bool m_isGhostHouse;
};

#endif // WORLDTILE_H
