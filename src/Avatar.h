#ifndef AVATAR_H
#define AVATAR_H

#include "MovableEntity.h"
#include "Vector2f.h"

// Entity implementation class to manage the player's avatar in the game.
class Avatar : public MovableEntity
{
public:
	// Construct the avatar at a given initial position. Pass Drawer so it can load its own images.
	Avatar(int xCoord, int yCoord, Drawer& drawer);
	// Avatar destructor; clean up any managed properties.
	~Avatar(void);

	// Update avatar state, given seconds passed since last update.
	void Update(float timeSecs, const World& world);

	// Avatar-specific logic for determining if we want to move over a tile.
	bool CanMoveToTile(int xCoord, int yCoord, const World& world) const;

private:
	// Image IDs for all of our visual states.
	int m_openImageIDs[4];
	int m_closedImageIDs[4];

	// Variables to aid determining which image to use. See Update.

	// Index of last image used, i.e. the direction.
	short m_lastImageIndex;
	// Time (seconds) since mouth opened or closed while moving.
	float m_timeSpentMoving;
};

#endif //AVATAR_H
