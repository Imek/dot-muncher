#include "Entity.h"
#include "Drawer.h"
#include "World.h"

Entity::Entity(float xCoord, float yCoord, int imageID, bool anIsActiveFlag)
: m_isActive(anIsActiveFlag)
, m_position(xCoord*World::TILE_SIZE, yCoord*World::TILE_SIZE)
, m_imageID(imageID)
{
}

Entity::~Entity(void)
{
}

void Entity::SetPosition(int toX, int toY)
{
	SetPosition(Vector2f(toX*World::TILE_SIZE, toY*World::TILE_SIZE));
}

bool Entity::Intersect(const Entity& aEntity, float distanceSq) const
{
	// Currently we just use a (optionally overridden) constant squared distance to check 
	// intersection with anything. If we wanted more generic and fine-grained detection,
	// we could store the image size on Entity and make use of that.
	return ((m_position - aEntity.GetPosition()).LengthSq() < distanceSq);
}

void Entity::Draw(Drawer& drawer) const
{
	if (m_isActive)
	{
		drawer.Draw(m_imageID, (int)m_position.m_xVal + 220, (int)m_position.m_yVal + 60);
	}
}
