#include "Ghost.h"
#include "World.h"
#include "WorldTile.h"
#include "Drawer.h"

Ghost::Ghost(
	int xCoord, int yCoord,
	int scatterTargetX, int scatterTargetY,
	GhostType ghostType,
	float houseTime,
	int aliveImageID,
	int frightenedImageID,
	int deadImageID,
	int flashingImageID)
: MovableEntity(xCoord, yCoord, aliveImageID, 0.f, 0, -1)
, m_curState(AT_HOME)
, m_flashFlag(false)
, m_ghostType(ghostType)
, m_scatterTargetX(scatterTargetX), m_scatterTargetY(scatterTargetY)
, m_aliveImageID(aliveImageID)
, m_frightenedImageID(frightenedImageID)
, m_deadImageID(deadImageID)
, m_flashingImageID(flashingImageID)
, m_targetTileX(0), m_targetTileY(0)
, m_initialHouseTime(houseTime)
, m_houseTimer(houseTime)
{
}

Ghost::~Ghost(void)
{
}

void Ghost::SetMovementTarget(int toX, int toY)
{
	m_targetTileX = toX;
	m_targetTileY = toY;
}

void Ghost::Die()
{
	// Only perform death logic if called when not already dead.
	if (m_curState != DEAD)
	{
		m_curState = DEAD;
		SetMovementTarget(World::GHOST_HOME_X, World::GHOST_HOME_Y);
	}
}

void Ghost::Update(float timeSecs, World& world)
{
	int curTileX = GetCurrentTileX();
	int curTileY = GetCurrentTileY();

	// UPDATE SPEED

	// Move slower when fleeing the player (claimable and not dead).
	if (m_curState == FRIGHTENED)
		m_speed = 90.f;
	// Else if in tunnel, speed should be 60.f.
	else if (world.TileIsTunnel(curTileX, curTileY))
		m_speed = 112.5f;
	// Full speed otherwise.
	else
		m_speed = 168.75f;


	// UPDATE HOUSE TIMER
	if (m_houseTimer > 0.f)
		m_houseTimer -= timeSecs;
	if (m_curState == AT_HOME && m_houseTimer <= 0.f)
		m_curState = ACTIVE;

	// UPDATE IMAGE

	// NOTE: Could just update the images when appropriate flags change, e.g. in Die,
	// but arguably this way is safer from logic bugs with negligible overhead.
	switch (m_curState)
	{
	case DEAD:
		m_imageID = m_deadImageID;
		break;
	case FRIGHTENED:
		m_imageID = m_flashFlag ? m_flashingImageID : m_frightenedImageID;
		break;
	default:
		m_imageID = m_aliveImageID;
	}


	// UPDATE MOVE DIRECTION (if applicable)

	if (IsAtDestination())
	{
		// We may pick a new movement direction; it gets set by reference if we do.
		World::MoveDirection targetDirection = GetDesiredMovingDirection();
		// Being frightened (random movement) overrides any pathing or other behaviour below.
		if (m_curState == FRIGHTENED)
		{
			world.GetRandomPath(*this, targetDirection);
		}
		// Mill about in the ghost house if still at home.
		else if (m_curState == AT_HOME)
		{
			switch (targetDirection)
			{
			case World::RIGHT:
			case World::DOWN:
				targetDirection = World::UP;
				break;
			case World::LEFT:
			case World::UP:
				targetDirection = World::DOWN;
				break;
			}
		}
		// Otherwise, if we have a target tile set on arriving at a tile, update our path.
		else if (m_targetTileX != curTileX || m_targetTileY != curTileY)
		{
			// Find a path. Should always find something, but set a default to be safe.
			// Note that we tell it to override with a random path if we're in the frightened state.
			world.GetPath(*this, m_targetTileX, m_targetTileY,	targetDirection);
		}
		// Otherwise, we don't have a new target; hopefully that means we've reached the
		// centre and can revive.
		else if (m_curState == DEAD)
		{
			m_curState = ACTIVE;
		}
		SetDesiredMovingDirection(targetDirection);
	} // IsAtDestination()

	// Call common movement code.
	MovableEntity::Update(timeSecs, world);
}

void Ghost::Reset()
{
	// Reset all members related to movement.
	SetDesiredMovingDirection(World::UP);
	m_houseTimer = m_initialHouseTime;
	m_flashFlag = false;
	m_curState = AT_HOME;
	// Do base movable entity reset.
	MovableEntity::Reset();
}

bool Ghost::CanMoveToTile(int toX, int toY, const World& world) const
{
	// Check base validity first.
	if (!MovableEntity::CanMoveToTile(toX, toY, world))
		return false;
	// Otherwise, the movement we set is allowed if waiting in the house.
	else if (m_curState == AT_HOME)
		return true;
	// Prevent ghost from trying to move into ghost house if active in the level.
	int curX = GetCurrentTileX();
	int curY = GetCurrentTileY();
	if (m_curState != DEAD && !world.TileIsGhostHouse(curX, curY) && world.TileIsGhostHouse(toX, toY))
		return false;

	// Normal ghost movement excludes reverse direction, so exclude the tile
	// behind current.
	// curX, curY being used as tile to exclude from this point.
	switch (GetMovingDirection())
	{
	case World::RIGHT:
		curX--;
		break;
	case World::DOWN:
		curY--;
		break;
	case World::LEFT:
		curX++;
		break;
	case World::UP:
		curY++;
		break;
	}
	return (toX != curX || toY != curY);
}
