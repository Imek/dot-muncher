#ifndef VECTOR2F_H
#define VECTOR2F_H

#include <math.h>
#include <assert.h>

// Class representing a 2-vector, with coordinates as floating point numbers.
// NOTE: Could make this a template class so that we can have a "Vector2d",
// avoiding the need to have all these "tileX, tileY" pairs.
class Vector2f
{
public:
	// Construct a zero-vector.
	Vector2f()
	{
		m_xVal = 0.f;
		m_yVal = 0.f;
	}

	// Construct a vector from specific number inputs.
	Vector2f(float xCoord, float yCoord)
	: m_xVal(xCoord)
	, m_yVal(yCoord)
	{
	}
	// By default, from an integer by dropping decimal part
	// (avoids having to cast in calling code to avoid compiler warnings)
	Vector2f(int xCoord, int yCoord)
	: m_xVal((float)xCoord)
	, m_yVal((float)yCoord)
	{
	}

	// Vector addition overload.
	inline const Vector2f Vector2f::operator+(const Vector2f &other) const 
	{
		Vector2f v(m_xVal + other.m_xVal, m_yVal + other.m_yVal);
		return v;
	}
	// In-place vector addition overload.
	inline Vector2f& Vector2f::operator+=(const Vector2f &other) 
	{
		m_xVal += other.m_xVal;
		m_yVal += other.m_yVal;

		return *this;
	}

	// Vector subtraction overload.
	inline const Vector2f Vector2f::operator-(const Vector2f &other) const 
	{
		Vector2f v(m_xVal - other.m_xVal, m_yVal - other.m_yVal);
		return v;
	}
	// In-place vector subtraction overload.
	inline Vector2f& Vector2f::operator-=(const Vector2f &other) 
	{
		m_xVal -= other.m_xVal;
		m_yVal -= other.m_yVal;

		return *this;
	}

	// Multiplication overload: multiply corresponding coordinates in two vectors
	inline const Vector2f Vector2f::operator*(const Vector2f& other) const 
	{
		Vector2f v(m_xVal * other.m_xVal, m_yVal * other.m_yVal);
		return v;
	}
	// Vector scalar multiplication overload.
	inline const Vector2f Vector2f::operator*(const float scalar) const 
	{
		Vector2f v(m_xVal*scalar, m_yVal*scalar);
		return v;
	}
	// In-place vector scalar multiplication overload.
	inline Vector2f& Vector2f::operator*=(const float scalar) 
	{
		m_xVal *= scalar;
		m_yVal *= scalar;

		return *this;
	}

	// Vector scalar division overload.
	inline const Vector2f Vector2f::operator/(const float value) const 
	{
		assert(value > 0.f);
		Vector2f v(m_xVal/value, m_yVal/value);
		return v;
	}
	// In-place vector scalar division overload.
	inline Vector2f& Vector2f::operator/=(const float scalar) 
	{
		assert(scalar > 0.f);
		m_xVal /= scalar;
		m_yVal /= scalar;

		return *this;
	}

	// Produce the vector's magnitude (length).
	inline float Vector2f::Length() const
	{
		return sqrt(m_xVal*m_xVal + m_yVal*m_yVal);
	}
	// Get the squared-length of the vector (faster than Length).
	inline float Vector2f::LengthSq() const
	{
		return m_xVal*m_xVal + m_yVal*m_yVal;
	}

	// Vector normalisation; make this a unit vector (unless it's a zero-vector).
	inline void Vector2f::Normalise()
	{
		float length = Length();
		if (length > 0.f)
			*this /= length;
	}

	// In-place vector dot product
	inline float DotProduct(const Vector2f& other) const
	{
		return m_xVal*other.m_xVal + m_yVal*other.m_yVal;
	}

	// Coordinate value members
	float m_xVal;
	float m_yVal;
};

#endif // VECTOR2F_H
