#include "WorldTile.h"
#include "World.h"

WorldTile::WorldTile(int xCoord, int yCoord, bool isBlocking, bool isTunnel, bool isGhostHouse)
: m_xCoord(xCoord)
, m_yCoord(yCoord)
, m_isBlocking(isBlocking)
, m_isTunnel(isTunnel)
, m_isGhostHouse(isGhostHouse)
{
}

WorldTile::~WorldTile(void)
{
}

Vector2f WorldTile::GetCentre() const
{
	return Vector2f(
		(float)(m_xCoord*World::TILE_SIZE) + (float)World::TILE_SIZE/2.f,
		(float)(m_yCoord*World::TILE_SIZE) + (float)World::TILE_SIZE/2.f);
}
