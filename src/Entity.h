#ifndef ENTITY_H
#define ENTITY_H

#include "Vector2f.h"

class Drawer;

// Base class for an entity that is drawn as an image in the game world.
// TODO: Needs a refactor for better separation of control, view and state code.
//
class Entity
{
public:
	// Construct a Entity, optionally given an ID of an image already loaded.
	// -1 indicates no image, so nothing wil be drawn.
	// By default an entity starts as active. Can specify the flag here otherwise.
	Entity(float xCoord, float yCoord, int imageID = -1, bool anIsActiveFlag = true);
	~Entity(void);

	Vector2f GetPosition() const { return m_position; }
	void SetPosition(const Vector2f& aPosition) { m_position = aPosition; }
	// Helper to set our current position given a set of tile coordinates.
	virtual void SetPosition(int toX, int toY);

	// Check whether this entity is intersecting another entity.
	bool Intersect(const Entity& aEntity, float distanceSq=25.f) const;
	// Draw this entity. Base behaviour simply draws the image at the current position.
	virtual void Draw(Drawer& drawer) const;

	bool m_isActive;

protected:
	Vector2f m_position;
	int m_imageID;
};

#endif // ENTITY_H
