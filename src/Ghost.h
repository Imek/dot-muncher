#ifndef GHOST_H
#define GHOST_H

#include <list>
#include "MovableEntity.h"

class World;
class WorldTile;

// Entity implementation class for a ghost enemy in the game.
class Ghost : public MovableEntity
{
public:
	// Use an enum to track ghost behavioural state.
	enum GhostState { AT_HOME, ACTIVE, FRIGHTENED, DEAD };
	
	// Enum that determines which ghost this is, mainly in terms of AI.
	enum GhostType { BLINKY, PINKY, INKY, CLYDE, GT_MAX };

	// Construct the ghost with given initial coordinates and configuration.
	Ghost(
		int xCoord, int yCoord,
		int aScatterX, int aScatterY,
		GhostType ghostType,
		float houseTime,
		int aliveImageID,
		int frightenedImageID,
		int deadImageID,
		int flashingImageID);
	// Ghost destructor; clean up any managed properties.
	~Ghost(void);

	// Update ghost state with game logic given time passed (seconds) and reference to game World instance for pathing.
	void Update(float timeSecs, World& world);

	// Set the ghost to try and navigate to the given position.
	void SetMovementTarget(int toX, int toY);
	// Accessors for the ghost's target coordinates.
	int GetMovementTargetX() const { return m_targetTileX; }
	int GetMovementTargetY() const { return m_targetTileY; }

	// Trigger death of this ghost (i.e. eaten by the player)
	void Die();
	// Set whether this ghost should be displayed with the "flashing" image.
	void SetFlash(bool anIsFlashingFlag);
	// Reset the ghost on player death or some other form of game reset.
	void Reset();

	// Ghost-specific logic for determining if we want to move over a tile.
	bool CanMoveToTile(int xCoord, int yCoord, const World& world) const;

	// Accessible members determining state (Generally to be managed by DotMuncher::Update)
	GhostState m_curState;
	bool m_flashFlag;
	const GhostType m_ghostType;

	// Configured movement target for scatter mode, used by GhostManager.
	const int m_scatterTargetX;
	const int m_scatterTargetY;

private:
	// Image IDs for other states. Entity.m_imageID is set to the one we currently want to draw.
	int m_aliveImageID;
	int m_frightenedImageID;
	int m_deadImageID;
	int m_flashingImageID;

	// Current intended movement target.
	int m_targetTileX;
	int m_targetTileY;

	// Timer to keep particular ghosts in the house at the start.
	// Note that original Pac-Man apparently has much more complicated logic
	// based on having dot counters. IMO this simpler way works pretty well.
	const float m_initialHouseTime;
	float m_houseTimer;
};

#endif // GHOST_H
