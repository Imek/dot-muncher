#include "GhostManager.h"
#include "World.h"
#include "Vector2f.h"

// Constants for convenient tweaking.
static const float FRIGHT_TIME = 8.0f;
static const float FRIGHT_FLASH_TIME = 3.0f;
static const float FRIGHT_FLASH_PERIOD = 0.3f;
// This governs the time sequence of alternating in and out of ghost scatter mode.
static const int NUM_SCATTER_STAGES = 7;
static const float SCATTER_SEQUENCE[NUM_SCATTER_STAGES] = { 7.f, 20.f, 7.f, 20.f, 5.f, 20.f, 5.f };
// Behavioural values
static const float CLYDE_SCATTER_DIST_SQ = pow(8.f*World::TILE_SIZE, 2);

GhostManager::GhostManager()
: m_ghosts(Ghost::GT_MAX)
, m_frightenedCounter(0.f)
// Until we run out of scatter states, and every time this counter reaches 0,
// it increments the state and sets the timer to the next one. 
, m_scatterCounter(0.f)
, m_scatterStateIndex(-1)
{

}

GhostManager::~GhostManager()
{
	// Clean up our ghosts.
	for (GhostVec::iterator it = m_ghosts.begin(); it != m_ghosts.end(); it++)
		delete *it;
}

bool GhostManager::Init(Drawer& drawer, const World& world)
{
	// Load all the images we'll need for ghosts.
	int redImageID = drawer.LoadImage("ghost_red_32.png");
	int pinkImageID = drawer.LoadImage("ghost_pink_32.png");
	int cyanImageID = drawer.LoadImage("ghost_cyan_32.png");
	int orangeImageID = drawer.LoadImage("ghost_orange_32.png");
	int frightenedImageID = drawer.LoadImage("ghost_frightened_32.png");
	int deadImageID = drawer.LoadImage("ghost_dead_32.png");
	int flashingImageID = drawer.LoadImage("ghost_flash_32.png");

	// Init the individual ghosts. They are indexed by their enum value.

	m_ghosts[Ghost::BLINKY] = new Ghost(
		World::GHOST_EXIT_X, World::GHOST_EXIT_Y,
		world.GetSizeX()-5, -5,
		Ghost::BLINKY,
		0.f,
		redImageID,
		frightenedImageID,
		deadImageID,
		flashingImageID);

	m_ghosts[Ghost::PINKY] = new Ghost(
		World::GHOST_HOME_X, World::GHOST_HOME_Y,
		5, -5,
		Ghost::PINKY,
		0.f,
		pinkImageID,
		frightenedImageID,
		deadImageID,
		flashingImageID);

	m_ghosts[Ghost::INKY] = new Ghost(
		World::GHOST_HOME_X-2, World::GHOST_HOME_Y,
		world.GetSizeX(), world.GetSizeY(),
		Ghost::INKY,
		4.f,
		cyanImageID,
		frightenedImageID,
		deadImageID,
		flashingImageID);

	m_ghosts[Ghost::CLYDE] = new Ghost(
		World::GHOST_HOME_X+2, World::GHOST_HOME_Y,
		0, world.GetSizeY(),
		Ghost::CLYDE,
		8.f,
		orangeImageID,
		frightenedImageID,
		deadImageID,
		flashingImageID);

	return true;
}

void GhostManager::Update(float timeSecs, World& world, Avatar& avatar)
{
	Ghost* thisGhost;
	bool finishedFright = false;
	bool flashOn = false;
	// Check ghost (big dot) counter.
	if (m_frightenedCounter > 0.f)
	{
		m_frightenedCounter -= timeSecs;
		// Ghost timer reached 0? Then ghosts must be reset.
		if (m_frightenedCounter <= 0.f)
		{
			finishedFright = true;
			flashOn = false;
		}
		// Ghost flashes when close to recovering.
		else if (m_frightenedCounter <= FRIGHT_FLASH_TIME)
		{
			flashOn = int(m_frightenedCounter/FRIGHT_FLASH_PERIOD) % 2 == 0;
		}
	}
	// If ghosts aren't in frightened mode, count down the scatter counter (if applicable).
	bool isScattering = false;
	if (m_frightenedCounter <= 0.f && m_scatterStateIndex <= NUM_SCATTER_STAGES)
	{
		m_scatterCounter -= timeSecs;
		if (m_scatterCounter <= 0.f)
		{
			m_scatterStateIndex++;
			if (m_scatterStateIndex < NUM_SCATTER_STAGES)
			{
				m_scatterCounter = SCATTER_SEQUENCE[m_scatterStateIndex];
			}
			else
			{
				m_scatterCounter = 0.f;
			}
		}
		// Toggle scattering state.
		isScattering = m_scatterStateIndex % 2 == 0;
	}

	// Update individual ghosts' target positions, as applicable.
	for (GhostVec::iterator it = m_ghosts.begin(); it != m_ghosts.end(); it++)
	{
		thisGhost = *it;
		// We first determine whether ghosts should flash, and whether they're exiting FRIGHTENED state.
		if (thisGhost->m_curState != Ghost::DEAD && finishedFright)
		{
			thisGhost->m_curState = Ghost::ACTIVE;
			thisGhost->m_flashFlag = false;
		}
		else
		{
			thisGhost->m_flashFlag = flashOn;
		}
		// Only update the ghost's target if it's active.
		if (thisGhost->m_curState == Ghost::ACTIVE)
		{
			UpdateGhostTarget(world, avatar, *thisGhost, isScattering);
		}
		// Always call its update function, obviously.
		thisGhost->Update(timeSecs, world);
	} // END FOR EACH GHOST
}

void GhostManager::Draw(Drawer& drawer) const
{
	for (GhostVec::const_iterator it = m_ghosts.begin(); it != m_ghosts.end(); it++)
	{
		(*it)->Draw(drawer);
	}
}

void GhostManager::Reset()
{
	// Reset all ghosts and any state variables that don't persist.
	Ghost* thisGhost;
	for (GhostVec::iterator it = m_ghosts.begin(); it != m_ghosts.end(); it++)
	{
		thisGhost = *it;
		thisGhost->Reset();
	}
	m_frightenedCounter = 0.f;
	// Reset scatter states back to start.
	// NOTE: In web pac man, this doesn't seem to reset.
	// But I've read that this does on losing a life in the original game..?
	m_scatterCounter = 0.f;
	m_scatterStateIndex = -1;
}

void GhostManager::OnEatenBigDot()
{
	// Any living ghosts become frightened for a set period if a big dot is eaten.
	Ghost* thisGhost;
	m_frightenedCounter = FRIGHT_TIME;
	for (GhostVec::iterator it = m_ghosts.begin(); it != m_ghosts.end(); it++)
	{
		thisGhost = *it;
		if (thisGhost->m_curState != Ghost::DEAD)
			thisGhost->m_curState = Ghost::FRIGHTENED;
	}
}

void GhostManager::UpdateGhostTarget(const World& world, const Avatar& avatar, Ghost& ghost, bool isScattering)
{
	// These will be changed if we decide we want to change the ghost's target tile.
	int newTargetX = ghost.GetMovementTargetX();
	int newTargetY = ghost.GetMovementTargetY();
	int curTileX = ghost.GetCurrentTileX();
	int curTileY = ghost.GetCurrentTileY();

	// Always exit the house first.
	if (world.TileIsGhostHouse(curTileX, curTileY))
	{
		newTargetX = World::GHOST_EXIT_X;
		newTargetY = World::GHOST_EXIT_Y;
	}
	// In scatter mode, ghosts target opposite corners of the map.
	else if (isScattering)
	{
		newTargetX = ghost.m_scatterTargetX;
		newTargetY = ghost.m_scatterTargetY;
	}
	// Otherwise, ghosts have their own per-type AI.
	else
	{
		int avatarTileX = avatar.GetCurrentTileX();
		int avatarTileY = avatar.GetCurrentTileY();
		// NOTE: If per-ghost code like this got much more complex, I'd consider refactoring
		// type-specific behaviour into a separate component with subclasses.
		if (ghost.m_ghostType == Ghost::BLINKY)
		{
			// Blinky always goes straight for the player.
			newTargetX = avatarTileX;
			newTargetY = avatarTileY;
		}
		else if (ghost.m_ghostType == Ghost::PINKY)
		{
			// Pinky tries to head the player off - target is 4 tiles ahead of player's movement.
			newTargetX = avatarTileX + avatar.GetCurrentMovementX()*4,
			newTargetY = avatarTileY + avatar.GetCurrentMovementY()*4;
		}
		else if (ghost.m_ghostType == Ghost::INKY)
		{
			// Inky is weird. He uses both the player's and Blinky's position.
			// The behaviour resulting here is that Inky will partner up with
			// Blinky to head off the player more aggressively the closer blinky gets.
			// NOTE: If we had Vector2d tile coordinates, this could be tidier.
			Vector2f calcVector = Vector2f(
				(float)(avatarTileX + avatar.GetCurrentMovementX()*2) * (float)World::TILE_SIZE,
				(float)(avatarTileY + avatar.GetCurrentMovementY()*2) * (float)World::TILE_SIZE);
			const Vector2f& blinkyPos = GetGhost(Ghost::BLINKY).GetPosition();
			calcVector -= blinkyPos;
			calcVector *= 2.f;
			calcVector += blinkyPos;
			newTargetX = (int)(calcVector.m_xVal/World::TILE_SIZE);
			newTargetY = (int)(calcVector.m_yVal/World::TILE_SIZE);
		}
		else if (ghost.m_ghostType == Ghost::CLYDE)
		{
			// Clyde targets the player, but then goes away if too close.
			if ((ghost.GetPosition() - avatar.GetPosition()).LengthSq() < CLYDE_SCATTER_DIST_SQ)
			{
				newTargetX = ghost.m_scatterTargetX;
				newTargetY = ghost.m_scatterTargetY;
			}
			else
			{
				newTargetX = avatarTileX;
				newTargetY = avatarTileY;
			}
		}
	} // END ACTIVE
	// Can now set target tile. Will only have changed if the above code decided to.
	ghost.SetMovementTarget(newTargetX, newTargetY);
}
