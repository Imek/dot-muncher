#ifndef DRAWER_H
#define DRAWER_H

#include <vector>
#include <assert.h>

// Forward declarations of SDL types
struct SDL_Surface;
struct SDL_Rect;
struct _TTF_Font;
typedef _TTF_Font TTF_Font;

// Convenience typedefs for the vectors we use to manage data in Drawer.
typedef std::vector<SDL_Surface*> SurfacePtrVec;
typedef std::vector<SDL_Rect*> RectPtrVec;
typedef std::vector<TTF_Font*> FontPtrVec;

// Class for an object that draws images and text to a particular SDL surface.
class Drawer
{
public:
	// Allocate and initialise a Drawer instance, returning its pointer. Returns null pointer if failed. 
	static Drawer* Create(SDL_Surface* window);
	// Destructor for drawer instance. Cleans up all of our managed data (pre-loaded surfaces etc)
	~Drawer(void);

	// Load an image given its path. Returns an image ID taken by Draw.
	int LoadImage(const char* imageFile);
	// Similarly, load and store a font (TTF_Font) given a file path.
	int LoadFont(const char* fontFile);

	// Draw an image given its ID from LoadImage.
	// Optional parameters xPos and yPos determine the X/Y coordinates to draw the image at,
	// if not the origin.
	void Draw(int imageID, int xPos = 0, int yPos = 0);
	// Draw some text, given a string of text and the font ID from LoadFont.
	// Parameters xPos and yPos determine the X/Y coordinates to draw the image at.
	void DrawText(const char* text, int fontID, int xPos, int yPos);

private:
	// Internal constructor for Drawer. 
	Drawer(SDL_Surface* window);
	// Private dummy copy/assignment operators for safety, since we're managing memory; we don't
	// want or need this functionality in this class.
	Drawer(const Drawer&);
	Drawer& operator=(const Drawer&);

	// Initialise this drawer on the window. Returns success flag.
	bool Init();
	
	SDL_Surface* m_window;

	// Stored surfaces loaded by LoadImage. An image ID is its index into this vector.
	SurfacePtrVec m_loadedSurfaces;
	RectPtrVec m_sizeRects;
	RectPtrVec m_posRects;
	// Stored fonts loaded by LoadFont.
	FontPtrVec m_loadedFonts;
};

#endif // DRAWER_H
