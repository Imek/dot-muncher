Introduction
============

Dot Muncher was a project that started out as a small programming exercise. I
have since extensively re-written or refactored all of the code, so all that
remains from the original code is some general structural elements and some SDL
boilerplate. I've also replaced the art with my own low-quality programmer art.

It's a pretty straightforward clone of Pac-Man, and currently only has one
level. I've mainly put it up as a more recent example of my C++ work, and as a
platform for learning SDL.

petter.mannerfelt@ubisoft.com should be credited for creating the original
basis code of this project.


Source Code
===========

Latest code can be found at https://bitbucket.org/Imek/dot-muncher. You can
use the Git version control system to check out the code to build and develop.


Development Info
================

This project supports CMake for generating environment-specific project files.
However, it's only so far been developed & tested in Visual Studio 2010.

http://www.cmake.org

VS2010 NOTE: You will need to set the Working Directory to $(TargetDir) for it
to find the data files without any modification.


Controls
========

Arrow keys.


Author & Licence
================

Name: Joe Forster
E-mail: me@joeforster.com
Website: http://joeforster.com/
Licence: See accompanying file LICENCE.txt
